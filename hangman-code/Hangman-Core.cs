﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hangman_logic
{
    class Hangman_Core
    {
        private List<string> Phrases;
        private List<char>   Solution;
        private List<char>   GuessedLetters;

        private string Phrase;
        private int HitPoints;
        private bool guessedWord;
        public bool IsPlaying { get { return guessedWord; } }
        public Hangman_Core()
        {
            Phrase = "Hello world";
            GuessedLetters = new List<char>(Phrase.Length);
            Solution       = Enumerable.Repeat(' ', Phrase.Length).ToList();             
            HitPoints      = Phrase.Length / 2;
            guessedWord    = false;
        }
        

        // Game Update Loop
        public void Update()
        {
            while (HitPoints > 0)
            {
                Console.WriteLine();
                Console.WriteLine("Guess a character...");
                // player guesses a character
                ConsoleKeyInfo Input = Console.ReadKey();

                // input validation
                if (char.IsLetterOrDigit(Input.KeyChar))
                {
                    Console.WriteLine();
                    // check that the character hasn't already been guessed
                    if (GuessedLetters.Contains(Input.KeyChar))
                    {
                        Console.WriteLine("Warning: You already guessed this character. ");
                    }
                    else
                    {
                        //
                        // if unique guess then check if it's a hit or miss
                        if (Phrase.Contains(Input.KeyChar.ToString()))
                        {
                            Console.WriteLine();
                            Console.WriteLine("You found a letter!!! ");
                            // add the letter to the guessed letters collection
                            GuessedLetters.Add(Input.KeyChar);
                            AddInputToSolution(Input.KeyChar);
                            PrintWord(Solution);
                            if (IsPhraseSolved()) { break; }
                        }
                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("Sorry, that character is not in the phrase.");                            
                            GuessedLetters.Add(Input.KeyChar);
                            HitPoints--;
                            Console.WriteLine(HitPoints + " tries remaining.");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Please enter a character or digit.");
                    continue;
                }
            }
        }
        private void PrintWord(List<char> Phrase)
        {
            foreach (char Letter in Phrase)
            {
                if (Letter == ' ')
                {
                    Console.Write('_');
                }
                else
                {
                    Console.Write(Letter);
                }
                Console.Write(' '); // white space for readability
            }
        }

        private void AddInputToSolution(char Input)
        {
            for (int i = 0; i < Phrase.Length; i++)
            {
                if (Phrase[i] == Input)
                {
                    // reveal it in the phrase
                    Solution[i] = Input;
                }
            }
        }

        private bool IsPhraseSolved()
        {
            if (string.Equals(Phrase, string.Concat(Solution)))
            {
                guessedWord = true;
                return true;                
            }
            return false;
        }

        public void StartGame()
        {
            Console.WriteLine("Welcome to hangman.");
            PrintWord(Solution);
        }

        public void EndGame()
        {
            // handle end of game
            if (guessedWord)
            {
                Console.WriteLine();
                Console.WriteLine("Congrats! You won.");
            }
            else
            {
                Console.WriteLine("Game over! You lose :C");
            }
        }
    }
}
