﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace hangman_logic
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Hangman_Core HangmanGame = new Hangman_Core();
            HangmanGame.StartGame();
            HangmanGame.Update();          

            // handle end of game
            if (HangmanGame.IsPlaying)
            {
                HangmanGame.EndGame();          
            } 
            Console.WriteLine("Press any key to exit.");
        }
    }
}
